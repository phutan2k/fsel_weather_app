import 'package:flutter/material.dart';

class ReusableIconWidget extends StatelessWidget {
  const ReusableIconWidget({
    Key? key,
    required this.iconData,
    this.backgroundColor = const Color(0x00000000),
    this.iconColor = const Color(0xFF000000),
    this.size = 40,
    this.iconSize = 16,
  }) : super(key: key);

  final IconData iconData;
  final Color backgroundColor;
  final Color iconColor;
  final double size;
  final double iconSize;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(size / 2),
        color: backgroundColor,
      ),
      child: Icon(
        iconData,
        color: iconColor,
        size: iconSize,
      ),
    );
  }
}
