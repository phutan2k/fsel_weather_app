import 'package:flutter/material.dart';

class ReusableTextWidget extends StatelessWidget {
  const ReusableTextWidget({
    Key? key,
    required this.text,
    this.color = const Color(0xFF000000),
    this.size = 14,
    this.fontWeight = FontWeight.w400,
  }) : super(key: key);

  final String text;
  final Color color;
  final double size;
  final FontWeight fontWeight;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: size,
        fontWeight: fontWeight,
      ),
    );
  }
}
