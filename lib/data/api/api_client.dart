import 'dart:convert';

import 'package:fsel_weather_app/models/current_weather/weather_model.dart';
import 'package:http/http.dart' as http;

import '../../utils/app_constants.dart';

class ApiClient {
  ApiClient();

  Future<WeatherModel?> getInfo({required String cityName}) async {
    try {
      final response = await http.get(Uri.parse(
        AppConstants.BASE_URL +
            '/current.json?key=' +
            AppConstants.API_KEY +
            '&q=' +
            cityName,
      ));

      if (response.statusCode == 200) {
        final data = json.decode(response.body);

        return WeatherModel.fromJson(data);
      } else {
        return null;
      }
    } catch (e) {
      print('ERROR ${e.toString()}');
      return null;
    }
  }
}
