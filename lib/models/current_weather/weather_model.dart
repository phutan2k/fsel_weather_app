import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:fsel_weather_app/models/current_weather/current.dart';
import 'package:fsel_weather_app/models/current_weather/location.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_model.g.dart';

@JsonSerializable()
class WeatherModel {
  final Location location;
  final Current current;

  WeatherModel({required this.location, required this.current});

  factory WeatherModel.fromJson(Map<String, dynamic> json) =>
      _$WeatherModelFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherModelToJson(this);
}
