// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_states.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$WeatherStatesCWProxy {
  WeatherStates isFahrenheit(bool isFahrenheit);

  WeatherStates model(WeatherModel? model);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WeatherStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WeatherStates(...).copyWith(id: 12, name: "My name")
  /// ````
  WeatherStates call({
    bool? isFahrenheit,
    WeatherModel? model,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfWeatherStates.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfWeatherStates.copyWith.fieldName(...)`
class _$WeatherStatesCWProxyImpl implements _$WeatherStatesCWProxy {
  final WeatherStates _value;

  const _$WeatherStatesCWProxyImpl(this._value);

  @override
  WeatherStates isFahrenheit(bool isFahrenheit) =>
      this(isFahrenheit: isFahrenheit);

  @override
  WeatherStates model(WeatherModel? model) => this(model: model);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `WeatherStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// WeatherStates(...).copyWith(id: 12, name: "My name")
  /// ````
  WeatherStates call({
    Object? isFahrenheit = const $CopyWithPlaceholder(),
    Object? model = const $CopyWithPlaceholder(),
  }) {
    return WeatherStates(
      isFahrenheit:
          isFahrenheit == const $CopyWithPlaceholder() || isFahrenheit == null
              ? _value.isFahrenheit
              // ignore: cast_nullable_to_non_nullable
              : isFahrenheit as bool,
      model: model == const $CopyWithPlaceholder()
          ? _value.model
          // ignore: cast_nullable_to_non_nullable
          : model as WeatherModel?,
    );
  }
}

extension $WeatherStatesCopyWith on WeatherStates {
  /// Returns a callable class that can be used as follows: `instanceOfWeatherStates.copyWith(...)` or like so:`instanceOfWeatherStates.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$WeatherStatesCWProxy get copyWith => _$WeatherStatesCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherStates _$WeatherStatesFromJson(Map<String, dynamic> json) =>
    WeatherStates(
      model: json['model'] == null
          ? null
          : WeatherModel.fromJson(json['model'] as Map<String, dynamic>),
      isFahrenheit: json['isFahrenheit'] as bool? ?? false,
    );

Map<String, dynamic> _$WeatherStatesToJson(WeatherStates instance) =>
    <String, dynamic>{
      'model': instance.model,
      'isFahrenheit': instance.isFahrenheit,
    };
