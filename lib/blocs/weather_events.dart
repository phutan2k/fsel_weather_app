import 'package:equatable/equatable.dart';

sealed class WeatherEvents extends Equatable {
  WeatherEvents();

  @override
  List<Object?> get props => [];
}

class InitWeatherEvent extends WeatherEvents {
  InitWeatherEvent();

  @override
  List<Object?> get props => [];
}

class ChooseLocationEvent extends WeatherEvents {
  final String cityName;

  ChooseLocationEvent(this.cityName);

  @override
  List<Object?> get props => [];
}

class TemperatureChangeEvent extends WeatherEvents {
  final bool isFahrenheit;

  TemperatureChangeEvent(this.isFahrenheit);

  @override
  List<Object?> get props => [];
}