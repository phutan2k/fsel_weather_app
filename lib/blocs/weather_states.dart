import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:fsel_weather_app/models/current_weather/weather_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_states.g.dart';

@JsonSerializable()
@CopyWith()
class WeatherStates extends Equatable {
  final WeatherModel? model;
  final bool isFahrenheit;

  WeatherStates({
    this.model,
    this.isFahrenheit = false,
  });

  @override
  List<Object?> get props => [
        model,
        isFahrenheit,
      ];

  factory WeatherStates.fromJson(Map<String, dynamic> json) =>
      _$WeatherStatesFromJson(json);

  Map<String, dynamic> toJson() => _$WeatherStatesToJson(this);
}
