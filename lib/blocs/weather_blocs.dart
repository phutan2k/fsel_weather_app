import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_weather_app/blocs/weather_events.dart';
import 'package:fsel_weather_app/blocs/weather_states.dart';
import 'package:fsel_weather_app/data/api/api_client.dart';

class WeatherBloc extends Bloc<WeatherEvents, WeatherStates> {
  WeatherBloc() : super(WeatherStates()) {
    on<InitWeatherEvent>(_onInitWeather);
    on<ChooseLocationEvent>(_onChooseLocationEvent);
    on<TemperatureChangeEvent>(_onTemperatureChangeEvent);
  }

  ApiClient apiClient = ApiClient();

  Future<FutureOr<void>> _onInitWeather(
    InitWeatherEvent event,
    Emitter<WeatherStates> emit,
  ) async {

    try {
      final data = await apiClient.getInfo(cityName: 'Ha Noi');

      if (data != null) {
        emit(state.copyWith(
          model: data,
        ));
      }
    } catch (e) {
      print('ERROR: ${e.toString()}');
    }
  }

  FutureOr<void> _onChooseLocationEvent(
    ChooseLocationEvent event,
    Emitter<WeatherStates> emit,
  ) async {

    try {
      final data = await apiClient.getInfo(cityName: event.cityName);

      if (data != null) {
        print(event.cityName);
        emit(state.copyWith(
          model: data,
        ));
      }
    } catch (e) {
      print('ERROR: ${e.toString()}');
    }
  }

  FutureOr<void> _onTemperatureChangeEvent(
    TemperatureChangeEvent event,
    Emitter<WeatherStates> emit,
  ) {
    emit(state.copyWith(isFahrenheit: event.isFahrenheit));
  }
}
