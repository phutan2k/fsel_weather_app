import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_weather_app/blocs/weather_blocs.dart';
import 'package:fsel_weather_app/blocs/weather_events.dart';
import 'package:fsel_weather_app/blocs/weather_states.dart';
import 'package:fsel_weather_app/widgets/reusable_icon_widget.dart';
import 'package:fsel_weather_app/widgets/reusable_text_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

const List<String> list = <String>['Ha Noi', 'Da Nang', 'Nha Trang'];

class _HomePageState extends State<HomePage> {
  bool _isFahrenheit = false;
  String dropdownValue = list.first;

  late String name;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<WeatherBloc>(context).add(
      InitWeatherEvent(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
          body: BlocBuilder<WeatherBloc, WeatherStates>(
            builder: (context, state) {
              if (state.model == null) {
                return Center(
                  child: CircularProgressIndicator(
                    color: Colors.red,
                  ),
                );
              } else {
                final Map<String, String> dataList = {
                  'Temperature': '${state.model!.current.tempC} celsius',
                  'Feel like': '${state.model!.current.feelslikeC} celsius',
                  'UV index': '${state.model!.current.uv}',
                  'Cloud': '${state.model!.current.cloud} %',
                  'Humidity': '${state.model!.current.tempC} %',
                  'Wind speed': '${state.model!.current.windKph} km/h',
                };
                return Container(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 20,
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          ReusableTextWidget(
                            text: 'Fahrenheit',
                            size: 16,
                            color: Colors.red,
                          ),
                          SizedBox(width: 5),
                          Switch(
                            value: state.isFahrenheit,
                            onChanged: (value) {
                              BlocProvider.of<WeatherBloc>(context)
                                  .add(TemperatureChangeEvent(value));
                            },
                          ),
                          Expanded(child: SizedBox()),
                          DropdownMenu<String>(
                            initialSelection: state.model?.location.name,
                            onSelected: (String? value) {
                              BlocProvider.of<WeatherBloc>(context)
                                  .add(ChooseLocationEvent(value!));
                            },
                            dropdownMenuEntries: list
                                .map<DropdownMenuEntry<String>>((String value) {
                              return DropdownMenuEntry<String>(
                                  value: value, label: value);
                            }).toList(),
                          )
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          ReusableTextWidget(
                            text: state.model!.location.name,
                            size: 28,
                            fontWeight: FontWeight.bold,
                          ),
                          SizedBox(width: 5),
                          state.model!.current.isDay == 1
                              ? ReusableIconWidget(
                                  iconData: Icons.sunny,
                                  iconSize: 20,
                                )
                              : ReusableIconWidget(
                                  iconData: Icons.nightlight_round,
                                  iconSize: 20,
                                ),
                        ],
                      ),
                      Image.network(
                        'https:' + state.model!.current.condition.icon,
                        width: 200,
                        height: 200,
                        fit: BoxFit.cover,
                      ),
                      ReusableTextWidget(
                        text:
                            'Last update: ${state.model!.current.lastUpdated}',
                      ),
                      SizedBox(height: 5),
                      ReusableTextWidget(
                        text:
                            'Description: ${state.model!.current.condition.text}',
                      ),
                      SizedBox(height: 50),
                      GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 10,
                          childAspectRatio: 2,
                          mainAxisSpacing: 8,
                        ),
                        itemBuilder: (_, index) {
                          if (state.isFahrenheit) {
                            dataList['Temperature'] =
                                '${state.model!.current.tempF} fahrenheit';
                            dataList['Feel like'] =
                                '${state.model!.current.feelslikeF} fahrenheit';
                          }
                          String key = dataList.keys.elementAt(index);
                          String value = dataList.values.elementAt(index);
                          return Container(
                            // color: Colors.red[50],
                            child: ReusableTextWidget(
                              text: '$key: $value',
                              size: 20,
                            ),
                          );
                        },
                        itemCount: dataList.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                      ),
                    ],
                  ),
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
